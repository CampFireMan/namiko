<?php

session_start();
require_once "inc/config.inc.php";
require_once "inc/functions.inc.php";

//Überprüfe, dass der User eingeloggt ist
//Der Aufruf von check_user() muss in alle internen Seiten eingebaut sein
$user = check_user();
check_consul();

include "templates/header.inc.php";
include "templates/nav.inc.php";
include "templates/admin-nav.inc.php";

require_once "inc/Template.php";

require_once "controller/CategoryController.php";
$controller = new CategoryController($pdo);


$categories = $controller->getCategories();
?>

<div class="container spacer">

  <a href="category.php?mode=create"><i class="fa fa-plus"></i>Neue Kategorie anlegen</a>
  <table class="table">
    <tr>
      <th>ID</th>
      <th>Sichtbar im Shop</th>
      <th>Name</th>
      <th class="text-right">Details</th>
    </tr>
    <?php foreach ($categories as $category) : ?>
    <tr>
      <td><?php e($category['cid']) ?></td>
      <th>
        <?php if($category['visible']) : ?>
        <i class="fa fa-check"></i>
        <?php else: ?>
        <i class="fa fa-times"></i>
        <?php endif; ?>
      </th>
      <td><?php e($category['category_name']) ?></td>
      <td class="text-right">
        <a href="category.php?mode=edit&id=<?php e($category['cid']) ?>">
          <i class="fa fa-edit" aria-hidden="true"></i> Bearbeiten
        </a>
        <a class="red" href="category.php?mode=delete&id=<?php e($category['cid']) ?>">
          <i class="fa fa-trash" aria-hidden="true"></i> Löschen
        </a>
      </td>
    </tr>
    <?php endforeach; ?>
  </table>
</div>

<?php
include "templates/footer.inc.php"
?>



