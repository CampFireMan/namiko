<?php

session_start();
require_once "inc/config.inc.php";
require_once "inc/functions.inc.php";

//Überprüfe, dass der User eingeloggt ist
//Der Aufruf von check_user() muss in alle internen Seiten eingebaut sein
$user = check_user();
check_consul();

require_once "controller/CategoryController.php";
$controller = new CategoryController($pdo);

$mode = $_REQUEST['mode'];
$error = "";
$visible = 0;

switch ($mode) {
    case 'edit':
        $id = intval($_REQUEST['id']);

        if ($_POST['submit'] ?? false) {
            $visible = $_POST["visible"] ?? 0;
            $controller->updateCategory($id, ["category_name" => $_POST["category_name"], "visible" => $visible, "attachable" => $_POST["attachable"]]);
        }
        $category = $controller->getCategory($id);
        $category_name = $category->category_name;
        $visible = $category->visible;
        break;
    case 'create':
        $id = '';
        $category_name = '';
        $category = null;
        if ($_POST['submit'] ?? false) {
            $category_name = $_POST["category_name"];
            $visible = $_POST["visible"] ?? 0;
            if ($controller->createCategory($category_name, $visible)) {
                header('Location: /categories.php');
            } else {
                $error = "Neue Kategorie kann nicht angelegt werden. Existiert bereits eine Kategorie mit diesem Namen oder enthält dieser unzulässige Zeichen?";
            }
        }
        break;
    case 'delete':
        $id = intval($_REQUEST['id']);
        $category = $controller->getCategory($id);
        $category_name = $category->category_name;
        $visible = $category->visible;
        if ($_POST['submit'] ?? false) {
            $controller->deleteCategory($id);
            header('Location: /categories.php');
        }
        break;
    default:
        e("Error, invalid mode");
        break;
}

include "templates/header.inc.php";
include "templates/nav.inc.php";
include "templates/admin-nav.inc.php";

require_once "inc/Template.php";

?>
<main class="spacer">
  <a href="/categories.php"><i class="fa fa-chevron-left" aria-hidden="true"></i> Zurück</a>
  <?php if ($mode === "delete"): ?>
  <div class="message white bg-danger">
    Soll diese Kategorie wirklich gelöscht werden? Dieser Vorgang ist nicht umkehrbar!
  </div>
  <?php endif; ?>
  <?php if ($error !== ""): ?>
  <div class="message bg-warning">
    <?php e($error); ?>
  </div>
  <?php endif; ?>
  <form method="post">
    <div class="form-group">
      <label for="id">ID</label>
      <input type="number" name="id" id="id" class="form-control" value="<?php e($id); ?>" readonly />
    </div>
    <div class="form-group">
      <label for="category_name">Name</label>
      <input 
        type="text"
        name="category_name"
        id="category_name"
        class="form-control"
        value="<?php e($category_name); ?>"
        <?php if ($mode === "delete"): ?>readonly<?php endif; ?>
      />
    </div>
    <div class="form-group">
      <label for="visible">Im Shop anzeigen?</label>
      <input
        type="checkbox"
        name="visible"
        id="visible"
        class="form-control"
        value="1" 
        <?php if ($visible): ?>checked <?php endif; ?> 
        <?php if ($mode === "delete"): ?>disabled<?php endif; ?>
      />
    </div>
    <div class="form-group">
      <label for="attachable">Erlaube Verbindung mit anderem Produkt (z.B. Pfand)</label>
      <input
        type="checkbox"
        name="attachable"
        id="attachable"
        class="form-control"
        value="1"
        <?php if ($category !== null && $category->attachable): ?>checked <?php endif; ?>
        <?php if ($mode === "delete"): ?>disabled<?php endif; ?>
      />
    </div>
    <?php if ($mode === "delete"): ?>
    <button type="submit" name="submit" class="btn btn-danger" value="1">Löschen</button>
    <?php else: ?>
    <button type="submit" name="submit" class="btn btn-primary" value="1">Speichern</button>
    <?php endif; ?>
  </form>
</main>
