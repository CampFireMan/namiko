ALTER TABLE categories
ADD visible bool DEFAULT true;
UPDATE categories SET visible=False WHERE category_name="Nicht anzeigen";
