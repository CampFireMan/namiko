ALTER TABLE categories ADD attachable bool DEFAULT false;
ALTER TABLE products ADD attached_product int(10) unsigned NULL;
ALTER TABLE products ADD FOREIGN KEY (attached_product) REFERENCES products (pid) ON DELETE SET NULL;
ALTER TABLE `order_items` ADD `tax` decimal(13,2) NOT NULL;
ALTER TABLE `order_items` ADD `netto_price` decimal(13,2) NOT NULL;
ALTER TABLE `preorder_items` ADD `tax` decimal(13,2) NOT NULL;
ALTER TABLE `preorder_items` ADD `netto_price` decimal(13,2) NOT NULL;
