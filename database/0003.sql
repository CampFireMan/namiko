CREATE TABLE `invoices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `paid` tinyint(1) NOT NULL DEFAULT 0,
PRIMARY KEY (`id`),
KEY `uid` (`uid`),
CONSTRAINT `invoices_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`)
);
ALTER TABLE `orders`
ADD `invoice` int(10) unsigned NULL,
ADD FOREIGN KEY (`invoice`) REFERENCES `invoices` (`id`);
INSERT INTO invoices(id, uid, paid) VALUES(1, 1, 1);
UPDATE orders SET invoice=1 WHERE created_at<"2023-11-23 18:00:02";
