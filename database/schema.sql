-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 30, 2023 at 02:09 PM
-- Server version: 10.6.14-MariaDB-cll-lve
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `turedev_namiko`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `cid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `categoryIMG` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`cid`),
  UNIQUE KEY `category_name_UNIQUE` (`category_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contributions`
--

CREATE TABLE IF NOT EXISTS `contributions` (
  `con_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(10) UNSIGNED NOT NULL,
  `pay_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`con_id`),
  KEY `fk_contributions_payments1_idx` (`pay_id`),
  KEY `fk_contributions_users1_idx` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `delivery_proof`
--

CREATE TABLE IF NOT EXISTS `delivery_proof` (
  `did` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `oid` int(10) UNSIGNED NOT NULL,
  `witness` int(10) UNSIGNED NOT NULL,
  `signature` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`did`),
  KEY `fk_delivery_proof_orders1_idx` (`oid`),
  KEY `fk_delivery_proof_users1_idx` (`witness`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `eid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` int(10) UNSIGNED NOT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`eid`),
  KEY `fk_events_types1_idx` (`type`),
  KEY `fk_events_users1_idx` (`created_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `event_types`
--

CREATE TABLE IF NOT EXISTS `event_types` (
  `tyid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `color` varchar(7) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`tyid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `inventory_items`
--

CREATE TABLE IF NOT EXISTS `inventory_items` (
  `ii_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(10) UNSIGNED NOT NULL,
  `quantity_KG_L` decimal(10,2) DEFAULT NULL,
  `last_edited_by` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`ii_id`),
  KEY `fk_iventory_items_products1_idx` (`pid`),
  KEY `fk_iventory_items_users1_idx` (`last_edited_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `inventory_log`
--

CREATE TABLE IF NOT EXISTS `inventory_log` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(10) UNSIGNED NOT NULL,
  `pid` int(10) UNSIGNED NOT NULL,
  `quantity` decimal(10,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `fk_inventory_log_products1_idx` (`pid`),
  KEY `fk_inventory_log_users1_idx` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `loans`
--

CREATE TABLE IF NOT EXISTS `loans` (
  `loan_id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(10) UNSIGNED NOT NULL,
  `pay_id` int(10) UNSIGNED NOT NULL,
  `recieved` tinyint(1) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`loan_id`),
  KEY `fk_loans_users1_idx` (`uid`),
  KEY `fk_loans_payments1_idx` (`pay_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mail_templates`
--

CREATE TABLE IF NOT EXISTS `mail_templates` (
  `temp_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `template` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `creator` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`temp_id`),
  KEY `fk_mail_templates_users1_idx` (`creator`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mandates`
--

CREATE TABLE IF NOT EXISTS `mandates` (
  `mid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(10) UNSIGNED NOT NULL,
  `ip` varbinary(16) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`mid`),
  KEY `fk_mandates_users_idx` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `newsletter_proof`
--

CREATE TABLE IF NOT EXISTS `newsletter_proof` (
  `id_proof` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `rid` int(10) UNSIGNED NOT NULL,
  `ip` varbinary(16) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_proof`),
  KEY `fk_newsletter_proof_newsletter_recipients1_idx` (`rid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `newsletter_recipients`
--

CREATE TABLE IF NOT EXISTS `newsletter_recipients` (
  `rid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `verified` tinyint(1) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `created_by` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`rid`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fk_newsletter_users1_idx` (`created_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE IF NOT EXISTS `notification` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_by` int(10) UNSIGNED NOT NULL,
  `text` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_notification_users1_idx` (`created_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `oid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `paid` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`oid`),
  KEY `fk_orders_users1_idx` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE IF NOT EXISTS `order_items` (
  `oi_id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(10) UNSIGNED NOT NULL,
  `oid` int(10) UNSIGNED NOT NULL,
  `delivered` tinyint(1) NOT NULL DEFAULT 0,
  `quantity` decimal(10,2) NOT NULL,
  `total` decimal(13,2) NOT NULL,
  PRIMARY KEY (`oi_id`),
  KEY `fk_order_items_products1_idx` (`pid`),
  KEY `oid` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_total`
--

CREATE TABLE IF NOT EXISTS `order_total` (
  `tid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `producer` int(10) UNSIGNED NOT NULL,
  `ordered_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `delivered` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `paid` tinyint(1) NOT NULL DEFAULT 0,
  `issued_by` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`tid`),
  KEY `fk_order_total_users1_idx` (`issued_by`),
  KEY `fk_order_total_producers1_idx` (`producer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_total_items`
--

CREATE TABLE IF NOT EXISTS `order_total_items` (
  `oti_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(10) UNSIGNED NOT NULL,
  `tid` int(10) UNSIGNED NOT NULL,
  `container` int(10) NOT NULL,
  `quantityContainer` int(10) NOT NULL,
  `total` decimal(13,2) NOT NULL,
  PRIMARY KEY (`oti_id`),
  KEY `fk_order_total_items_order_total1_idx` (`tid`),
  KEY `fk_order_total_items_products1_idx` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `pay_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(10) UNSIGNED NOT NULL,
  `reference` int(10) UNSIGNED NOT NULL,
  `amount` decimal(13,2) NOT NULL,
  PRIMARY KEY (`pay_id`),
  KEY `fk_payments_users1_idx` (`uid`),
  KEY `fk_payments_sepaDocs1_idx` (`reference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `preorders`
--

CREATE TABLE IF NOT EXISTS `preorders` (
  `oid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`oid`),
  KEY `fk_preorders_users1_idx` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `preorder_items`
--

CREATE TABLE IF NOT EXISTS `preorder_items` (
  `oi_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(10) UNSIGNED NOT NULL,
  `oid` int(10) UNSIGNED NOT NULL,
  `quantity` decimal(10,2) NOT NULL,
  `total` decimal(13,2) NOT NULL,
  `transferred` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`oi_id`),
  KEY `fk_preorder_items_preorders1_idx` (`oid`),
  KEY `fk_preorder_items_products1_idx` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `producers`
--

CREATE TABLE IF NOT EXISTS `producers` (
  `pro_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `producerName` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `description` text DEFAULT NULL,
  PRIMARY KEY (`pro_id`),
  UNIQUE KEY `producerName_UNIQUE` (`producerName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `pid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `productName` varchar(255) NOT NULL,
  `productDesc` text DEFAULT NULL,
  `netto` decimal(13,2) DEFAULT NULL,
  `price_KG_L` decimal(13,2) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `tax` decimal(5,2) DEFAULT 0.07,
  `unit_size` decimal(10,2) UNSIGNED DEFAULT 1.00,
  `unit_tag` varchar(10) DEFAULT 'KG',
  `category` int(10) UNSIGNED NOT NULL,
  `container` decimal(7,2) DEFAULT NULL,
  `priceContainer` decimal(13,2) UNSIGNED NOT NULL,
  `origin` varchar(255) DEFAULT NULL,
  `producer` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `is_storage_item` tinyint(4) DEFAULT 0,
  `last_price` decimal(13,2) DEFAULT NULL,
  PRIMARY KEY (`pid`),
  KEY `fk_products_categories1_idx` (`category`),
  KEY `fk_products_producers1_idx` (`producer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_price_log`
--

CREATE TABLE IF NOT EXISTS `product_price_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(10) UNSIGNED NOT NULL,
  `price_per_unit_brutto` decimal(13,2) DEFAULT NULL,
  `date` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `fk_product_price_log_products1_idx` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `securitytokens`
--

CREATE TABLE IF NOT EXISTS `securitytokens` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `securitytoken` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `fk_securitytokens_users1_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sepaDocs`
--

CREATE TABLE IF NOT EXISTS `sepaDocs` (
  `sid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `PmtInfId` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `creator` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`sid`),
  UNIQUE KEY `PmtInfId_UNIQUE` (`PmtInfId`),
  KEY `fk_sepaDocs_users1_idx` (`creator`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `uid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `passwordcode` varchar(255) DEFAULT NULL,
  `passwordcode_time` timestamp NULL DEFAULT NULL,
  `rights` int(10) NOT NULL DEFAULT 0,
  `organization` varchar(200) DEFAULT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `postal_code` int(10) NOT NULL,
  `region` varchar(255) NOT NULL,
  `street` varchar(255) NOT NULL,
  `street_number` int(10) NOT NULL,
  `account_holder` varchar(255) NOT NULL,
  `IBAN` varchar(34) NOT NULL,
  `BIC` varchar(15) NOT NULL,
  `contribution` int(10) NOT NULL DEFAULT 5,
  `loan` int(10) UNSIGNED NOT NULL,
  `notification` tinyint(1) DEFAULT 0,
  `newsletter` tinyint(1) UNSIGNED DEFAULT 1,
  `verify_code` varchar(8) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`uid`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;


--
-- Constraints for dumped tables
--

--
-- Constraints for table `delivery_proof`
--
ALTER TABLE `delivery_proof`
  ADD CONSTRAINT `fk_delivery_proof_orders1` FOREIGN KEY IF NOT EXISTS (`oid`) REFERENCES `orders` (`oid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_delivery_proof_users1` FOREIGN KEY IF NOT EXISTS (`witness`) REFERENCES `users` (`uid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `order_items`
--
ALTER TABLE `order_items`
  ADD CONSTRAINT `order_items_ibfk_1` FOREIGN KEY IF NOT EXISTS (`oid`) REFERENCES `orders` (`oid`) ON DELETE CASCADE;
COMMIT;
