<?php

require_once "../inc/config.inc.php";

$statement = $pdo->prepare("SELECT * from order_items LEFT JOIN products ON order_items.pid=products.pid");
$statement->execute();

while($item = $statement->fetch()) {

    echo "Total ";
    echo $item['total'];
    echo "<br>";
    echo "Quantity ";
    echo $item['quantity'];
    echo "<br>";
    echo "Tax ";
    echo $item['tax'];
    echo "<br>";

    if($item['quantity'] != 0) {
        $brutto_per_item = $item['total'] / $item['quantity'];
    } else {
        continue;
    }
    echo "Brutto ";
    echo $brutto_per_item;
    echo "<br>";

    $netto_per_item = $brutto_per_item - ($brutto_per_item / (1 + $item['tax']) * $item['tax']);
    echo "Netto ";
    echo ceil($netto_per_item * 100) / 100;

    echo "<br>Kontroll-Wert: ";
    $control = $netto_per_item * (1 + $item["tax"]) * $item["quantity"];
    echo $control;

    if(!$control === $item["total"]) {
        echo "<h1>Fail!</h1>";
    } else {
        $stmnt = $pdo->prepare("UPDATE order_items SET netto_price=:price, tax=:tax WHERE oi_id=:oi_id");
        $stmnt->bindParam("price", $netto_per_item);
        $stmnt->bindParam("tax", $item['tax']);
        $stmnt->bindParam("oi_id", $item["oi_id"]);
        $stmnt->execute();
    }

    echo "<br><hr>";
}
