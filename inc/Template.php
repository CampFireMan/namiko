<?php

include(__DIR__."/config.inc.php");

function e(string $s): void
{
    echo htmlspecialchars($s);
}

function currency(float $amount): void
{
    global $currency_symbol;
    $amount = ceil($amount * 100) / 100;
    echo htmlspecialchars($amount . " " . $currency_symbol);
}

function datetime(string $datetime): void
{
    $date = new DateTime($datetime);
    echo htmlspecialchars($date->format("d.m.Y H:i"));
}

function day(string $datetime): void
{
    $date = new DateTime($datetime);
    echo htmlspecialchars($date->format("d.m.Y"));
}

function percent(float $percent): void
{
    if($percent >= 0 && $percent <= 1) {
        echo htmlspecialchars($percent * 100 . "%");
    } else {
        echo htmlspecialchars($percent . "%");

    }
}

/**
 * For a given $string, mask all chars from $start
**/
function mask($string, $start = 0)
{
    $char_to_mask = strlen($string) - $start;
    return substr_replace($string, str_repeat("X", $char_to_mask), $start);
}
