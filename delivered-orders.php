<?php
session_start();
require_once "inc/config.inc.php";
require_once "inc/functions.inc.php";

//Überprüfe, dass der User eingeloggt ist
//Der Aufruf von check_user() muss in alle internen Seiten eingebaut sein
$user = check_user();
check_consul();

include "templates/header.inc.php";
include "templates/nav.inc.php";
include "templates/admin-nav.inc.php";

require_once "inc/Template.php";

require_once "controller/OrderController.php";
$controller = new OrderController($pdo);

$endIndex = intval($_GET['endIndex'] ?? 0);

$deliveredOrders = $controller->getDeliveredOrders($endIndex);
?>

<div class="container">

  <form method="GET">
    <label for="endIndex">obere Grenze für die Bestellnummer</label>
    <input type="number" name="endIndex" value="<?php e($endIndex) ?>">
    <input type="submit" name="Laden" value="Laden">
    <a href="delivered-orders.php">Neuste anzeigen</a>
  </form>
  <?php foreach ($deliveredOrders as $order) : ?>
  <div class="order">
    <div class="subtitle2">
      <span>Bestellung #<?php e($order['oid']) ?></span>
    </div>
    <div class="inline"><?php e($order['first_name'] . ' ' . $order['last_name']) ?></div>
    <div class="subtitle3 inline" style="float: right">
      <span><?php datetime($order['created_at']) ?></span>
    </div>
    <br>
    <br>
    <table class="table">
      <tr style="text-align: left;">
        <th>Artikel</th>
        <th>Preis E</th>
        <th>Menge</th>
        <th>&#931;</th>
      </tr>

      <?php

      $orderItems = $controller->getOrderItems($order['oid']);
      foreach ($orderItems as $orderItem) : ?>
      
      <tr>
        <td><?php e($orderItem['productName']) ?></td>
        <td><?php currency($orderItem['price_KG_L']) ?></td>
        <td><?php e($orderItem['quantity']) ?></td>
        <td><?php currency($orderItem['quantity'] * $orderItem['price_KG_L']) ?></td>
      </tr>
      <?php endforeach; ?>

      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td class="emph">
          <?php currency($controller->getOrderItemsTotal()) ?>
        </td>
      </tr>
    </table>
  </div>
  <?php endforeach; ?>
  <div class="pagination"><a href="delivered-orders.php?endIndex=<?php e($controller->getLastOrderID() - 1); ?>">ältere anzeigen</a></div>
</div>

<?php
include "templates/footer.inc.php"
?>

