<?php

class CategoryController
{
    protected $pdo; // TODO We need to encapsulate all database operations
    private $orderItemsTotal = 0;
    private $lastOrderID = null;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    public function getCategories(): array
    {
        $statement = $this->pdo->prepare("SELECT * from categories");
        $statement->execute();
        $categories = $statement->fetchAll();
        return $categories;
    }

    public function getCategory(int $id)
    {
        $statement = $this->pdo->prepare("SELECT * FROM categories WHERE cid = :id");
        $statement->execute(['id' => $id]);
        if ($statement->rowCount() != 1) {
            throw new Exception("Invalid amount of rows found");
        }
        return $statement->fetchObject();
    }

    public function updateCategory(int $id, array $values)
    {
        $allowed = ["category_name", "visible", "attachable"];
        $setStr = "";
        $params = [];

        foreach ($allowed as $key) {
            $setStr .= "`$key` = :$key,";
            $params[$key] = $values[$key];
        }
        $setStr = rtrim($setStr, ",");

        $params['id'] = $id;
        $statement = $this->pdo->prepare("UPDATE categories SET $setStr WHERE cid = :id");
        $statement->execute($params);
    }

    public function createCategory(string $category_name, int $visible = 0): bool
    {
        $params["category_name"] = $category_name;
        $params["visible"] = $visible;
        $statement = $this->pdo->prepare("INSERT INTO categories (category_name, visible) VALUES (:category_name, :visible)");
        try {
            $statement->execute($params);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function deleteCategory(int $id)
    {
        $params["cid"] = $id;
        $statement = $this->pdo->prepare("DELETE FROM categories WHERE cid = :cid");
        $statement->execute($params);
    }
}
