<?php

class ProductController
{
    protected $pdo; // TODO We need to encapsulate all database operations
    private $orderItemsTotal = 0;
    private $lastOrderID = null;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    public function getProducts(): array
    {
    }

    public function getProduct(int $id)
    {
        $statement = $this->pdo->prepare("SELECT * FROM products WHERE pid = :id");
        $statement->execute(['id' => $id]);
        if ($statement->rowCount() != 1) {
            throw new Exception("Invalid amount of rows found");
        }
        return $statement->fetchObject();
    }

    public function updateProduct(int $id, array $values)
    {
        $allowed = [
          "productName",
          "productDesc",
          "netto",
          "price_KG_L",
          "tax",
          "unit_size",
          "unit_tag",
          "category",
          "container",
          "priceContainer",
          "origin",
          "producer",
          "is_storage_item",
          "attached_product"
        ];
        $setStr = "";
        $params = [];

        foreach ($allowed as $key) {
            $setStr .= "`$key` = :$key,";
            if($values[$key] === "") {
                $params[$key] = null;
            } else {
                $params[$key] = $values[$key];
            }
        }
        $setStr = rtrim($setStr, ",");

        $params['id'] = $id;
        $statement = $this->pdo->prepare("UPDATE products SET $setStr WHERE pid = :id");
        $statement->execute($params);
    }

    public function createProduct(string $category_name, int $visible = 0): bool
    {
        $params["category_name"] = $category_name;
        $params["visible"] = $visible;
        $statement = $this->pdo->prepare("INSERT INTO categories (category_name, visible) VALUES (:category_name, :visible)");
        try {
            $statement->execute($params);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function deleteProduct(int $id)
    {
        $params["cid"] = $id;
        $statement = $this->pdo->prepare("DELETE FROM categories WHERE cid = :cid");
        $statement->execute($params);
    }

    public function getStatistics(int $id)
    {
        $statement = $this->pdo->prepare(
            "
        SELECT SUM(oi.quantity) * p.unit_size AS sum, p.unit_tag, YEAR(o.created_at) AS year, MONTH(o.created_at) AS month
        FROM order_items AS oi
        LEFT JOIN orders AS o ON o.oid = oi.oid
        LEFT JOIN products AS p on oi.pid = p.pid
        WHERE oi.pid = :pid
        GROUP BY YEAR(o.created_at), MONTH(o.created_at)
      "
        );
        $statement->execute(['pid' => $id]);
        return $statement->fetchAll();

    }

    public function getAttachables()
    {
        $statement = $this->pdo->prepare("SELECT * FROM products LEFT JOIN categories ON products.category=categories.cid WHERE attachable=True;");
        $statement->execute();
        return $statement->fetchAll();
    }
}
