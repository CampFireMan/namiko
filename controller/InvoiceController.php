<?php

class InvoiceController
{
    protected $pdo; // TODO We need to encapsulate all database operations
    private $orderItemsTotal = 0;
    private $lastOrderID = null;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    public function getInvoices(): array
    {
        $statement = $this->pdo->prepare("SELECT * from invoices LEFT JOIN users on users.uid=invoices.uid ORDER BY id DESC");
        $statement->execute();
        $invoices = $statement->fetchAll();
        return $invoices;
    }

    public function getInvoice(int $id)
    {
        $statement = $this->pdo->prepare("SELECT * FROM invoices WHERE id = :id");
        $statement->execute(['id' => $id]);
        if ($statement->rowCount() != 1) {
            throw new Exception("Invalid amount of rows found");
        }
        return $statement->fetchObject();
    }

    public function getInvoicableUsers(int $limit = 0): array
    {
        // Get all invoicable users
        $query = "SELECT DISTINCT 
    users.uid, users.organization, users.first_name, users.last_name, users.street, users.street_number, users.postal_code, users.region, users.email 
    FROM users 
    LEFT JOIN orders ON users.uid=orders.uid
    WHERE orders.invoice IS NULL AND users.first_name != 'deleted' AND orders.uid IS NOT NULL";
        if ($limit > 0) {
            $query .= " LIMIT " . strval($limit);
        }
        $statement = $this->pdo->prepare($query);
        $result = $statement->execute();
        if (!$result) {
            throw new Exception('Unable to fetch unpaid orders');
        }
        return $statement->fetchAll();
    }

    public function getInvoicableOrdersByUser(int $id): array
    {
        // Read open order_items from DB
        $statement_oi = $this->pdo->prepare("
      SELECT *, order_items.netto_price AS netto, products.productName FROM order_items
      LEFT JOIN orders ON order_items.oid = orders.oid
      LEFT JOIN products ON order_items.pid = products.pid
      WHERE orders.invoice IS NULL AND orders.uid = :uid");
        $result = $statement_oi->execute(array('uid' => $id));
        $order_items = $statement_oi->fetchAll();

        if (!$result) {
            throw new Exception('Unable to fetch invoicable orders for user ' . $id);
        }
        return $order_items;
    }

    public function createInvoice(int $id): int
    {
        $this->pdo->prepare("INSERT INTO invoices(uid) VALUES(:uid)")->execute(array('uid' => $id));
        $invoice_statement = $this->pdo->prepare("SELECT * FROM invoices WHERE uid=:uid ORDER BY id DESC LIMIT 1;");
        $invoice_statement->execute(array('uid' => $id));
        $invoice = $invoice_statement->fetch();
        return $invoice['id'];
    }

    public function markAsInvoiced(int $order_id, int $invoice_id): void
    {
        $this->pdo->prepare("UPDATE orders SET invoice=:invoice_id WHERE oid=:oid")->execute(array('invoice_id' => $invoice_id, 'oid' => $order_id));
    }

    public function getUnnotifiedInvoices(): array
    {
        $statement = $this->pdo->prepare("
      SELECT
      invoices.id,
      invoices.date,
      users.uid,
      users.first_name,
      users.last_name,
      users.email,
      users.IBAN,
      users.BIC,
      mandates.mid,
      SUM(order_items.quantity*CEILING((1+order_items.tax)*order_items.netto_price*100)/100) AS total FROM invoices
    LEFT JOIN orders on invoices.id = orders.invoice
    LEFT JOIN order_items on order_items.oid = orders.oid
    LEFT JOIN users on invoices.uid = users.uid
    LEFT JOIN mandates on invoices.uid = mandates.uid
    WHERE invoices.notified=0 AND invoices.paid=1 AND invoices.id != 1
    GROUP BY id LIMIT 5");
        $statement->execute();
        return $statement->fetchAll();
    }

    public function setNotified(int $invoice_id): void
    {
        $this->pdo->prepare("UPDATE invoices SET notified=1 WHERE id=:id")->execute(array('id' => $invoice_id));
    }
}
