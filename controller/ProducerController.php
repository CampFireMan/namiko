<?php

class ProducerController
{
    protected $pdo; // TODO We need to encapsulate all database operations

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    public function getProducers(): array
    {
        $statement = $this->pdo->prepare("SELECT * from producers");
        $statement->execute();
        $producers = $statement->fetchAll();
        return $producers;
    }
}
