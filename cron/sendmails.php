<?php

require_once __DIR__ . "/../inc/config.inc.php";

require_once __DIR__ . "/../inc/functions.inc.php";
require_once __DIR__ . "/../inc/SEPAprocedure.inc.php";

require __DIR__ . '/../vendor/autoload.php';

require_once __DIR__ . "/../controller/InvoiceController.php";
$controller = new InvoiceController($pdo);
$invoices = $controller->getUnnotifiedInvoices();

foreach($invoices as $invoice) {

    $mail = new Mail($smtp_host, $smtp_username, $smtp_password, $myEmail, $myEntity);

    $invoice_path = __DIR__ . "/../invoices/Rechnung_" . $invoice['id'] . ".pdf";

    ob_start();
    include(__DIR__."/../templates/invoice_email.php");
    $text = ob_get_contents();
    ob_end_clean();

    $mail->send($invoice['email'], $invoice['first_name'] . " " . $invoice['last_name'], "Rechnung " . $invoice['id'], $text, false, $invoice_path);
    $controller->setNotified($invoice['id']);
}
