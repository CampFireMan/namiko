<div class="admin-nav">
  <div class="limiter">
    <ul>
      <li id="inventory"><a href="inventory.php">Inventar</a></li>
      <li id="order_total"><a href="order_total.php">Bestellungen</a></li>
      <li id="delivered-orders"><a href="delivered-orders.php">ausgegeben</a></li>
      <?php if($user['rights'] >= 4): ?>
      <li id="admin"><a href="admin.php">Katalog</a></li>
      <li id="producers"><a href="producers.php">Lieferant</a></li>
      <li id="members"><a href="members.php">Mitglieder</a></li>
      <li id="invoices"><a href="invoices.php">Rechnungen</a></li>
      <li id="emailcenter"><a href="emailcenter.php">EmailCenter</a></li>
      <?php endif; ?>
      <li id="calendar"><a href="calendar.php">Kalender</a></li>
      <li id="session"><a href="session.php">offene Bestellungen</a></li>
      <li><a href="categories.php">Kategorien</a></li>
    </ul>
  </div>
</div>
<script type="text/javascript" src="js/nav.js"></script>
