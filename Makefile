install:
	composer install
test:
	[ ! -f inc/config.inc.php ] || mv inc/config.inc.php inc/config.bak
	cp inc/config.inc.test.php inc/config.inc.php
	./vendor/bin/phpunit tests/ --coverage-text --display-warnings --log-junit test-report.xml
	[ ! -f inc/config.bak ] || mv inc/config.bak inc/config.inc.php

package:
	git diff --quiet || (echo "Working directory is dirty!" && exit 1)
	git describe --tags > inc/version.txt
	rm -rf vendor && mkdir vendor
	rm -rf dist && mkdir dist
	zip -q dist/namiko.zip -r . -x "*/.*" -x ".*" -x "dist**"
